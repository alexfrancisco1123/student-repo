*Solution 

- The setup.yml playbook will configure the all of the necessary controller constructs up to 4-network-compliance.

```bash
$ ansible-navigator run setup.yml -m stdout -e "username=your-gitlab-user token=your-gitlab-token password=your-lab-student-password"