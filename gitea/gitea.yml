---
# Playbook intended to be deployed on a Red Hat Ansible Network Workshop Control Node
- name: Install Gitea in a Container on the Ansible Control Node
  hosts: gitea
  gather_facts: False
  become: true

  tasks:
    - name: Install Podman
      ansible.builtin.dnf:
        name: podman
        state: installed

    - name: Create the directory for Gitea
      ansible.builtin.file:
        path: /root/gitea_data
        state: directory
        mode: '0755'
    
    - name: Run gitea container
      containers.podman.podman_container:
        name: gitea
        image: docker.io/gitea/gitea:latest
        volumes:
          - /root/gitea_data:/data:Z
        state: started
        ports:
          - "3000:3000"
    
    - name: Update Settings in the Gitea Container
      block:
      - name: Create gitea database
        ansible.builtin.shell:
          cmd: podman exec --user git gitea touch /data/gitea/gitea.db
    
      - name: Set the ROOT_URL
        ansible.builtin.shell:
          cmd: podman exec --user git gitea sed -i -e "s/^ROOT_URL *= $/ROOT_URL = https:\/\/{{ ansible_host }}\/gitea\//" /data/gitea/conf/app.ini

      - name: Include Allowed Domains for Migration
        ansible.builtin.shell:
          cmd: podman exec --user git gitea sed -i '$ a\\n[migrations]\nALLOWED_DOMAINS = gitlab.com' /data/gitea/conf/app.ini
          #cmd: podman exec --user git gitea echo $'\n[migrations]\nALLOWED_DOMAINS = gitlab.com' >> >> /data/gitea/conf/app.ini

      - name: Lock app.ini
        ansible.builtin.shell:
          cmd: podman exec --user git gitea sed -i -e "s/^INSTALL_LOCK.*/INSTALL_LOCK=true/" /data/gitea/conf/app.ini

      - name: Restart gitea
        ansible.builtin.shell:
          cmd: podman restart gitea
      #### END OF BLOCK ####

    - name: Update NGINX when it exists | for workshop control node
      block:
      - name: Update NGINX to allow for Gitea url
        ansible.builtin.blockinfile:
          dest: /etc/nginx/nginx.conf
          insertafter: '# BEGIN ANSIBLE MANAGED BLOCK'
          backup: true
          block: |
              location /editor/ {
                  proxy_pass http://127.0.0.1:8080/;
                  proxy_set_header Host $host;
                  proxy_set_header Upgrade $http_upgrade;
                  proxy_set_header Connection upgrade;
                  proxy_set_header Accept-Encoding gzip;
                  proxy_redirect off;
              }
              location /gitea/ {
                  client_max_body_size 512M;
                  # make nginx use unescaped URI, keep "%2F" as is
                  rewrite ^ $request_uri;
                  rewrite ^/gitea(/.*) $1 break;
                  proxy_pass http://127.0.0.1:3000$uri;
                  proxy_set_header Connection $http_connection;
                  proxy_set_header Upgrade $http_upgrade;
                  proxy_set_header Host $host;
                  proxy_set_header X-Real-IP $remote_addr;
                  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                  proxy_set_header X-Forwarded-Proto $scheme;
              }
      
      - name: Restart NGINX service
        ansible.builtin.service:
          name: nginx
          state: restarted
      when:
      - lookup('file', '/etc/nginx/nginx.conf', errors='warn')
      ### END OF BLOCK ###
