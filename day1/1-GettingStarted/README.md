# Exercise 1 - Workshop Getting Started Guide

## Table of Contents

- [Exercise 1 - Workshop Getting Started Guide](#exercise-1---workshop-getting-started-guide)
  - [Table of Contents](#table-of-contents)
  - [Objective](#objective)
  - [Diagram](#diagram)
  - [Guide](#guide)
    - [Step 1 - Connecting via VS Code](#step-1---connecting-via-vs-code)
    - [Step 2 - Using the Terminal](#step-2---using-the-terminal)
    - [Step 3 - Create a Git repository](#step-3---create-a-git-repository)
    - [Step 4 - Create a Token](#step-4---create-a-token)
    - [Step 5 - Clone the Repository:](#step-5---clone-the-repository)
    - [Step 6 - Open the project directory in Visual Studio Code](#step-6---open-the-project-directory-in-visual-studio-code)
  - [Complete](#complete)

## Objective

These first few lab exercises will be exploring the command-line utilities of the Ansible Automation Platform.  This includes:

- [ansible-navigator](https://github.com/ansible/ansible-navigator) - a command line utility and text-based user interface (TUI) for running and developing Ansible automation content.
- [ansible-core](https://docs.ansible.com/core.html) - the base executable that provides the framework, language and functions that underpin the Ansible Automation Platform.  It also includes various cli tools like `ansible`, `ansible-playbook` and `ansible-doc`.  Ansible Core acts as the bridge between the upstream community with the free and open source Ansible and connects it to the downstream enterprise automation offering from Red Hat, the Ansible Automation Platform.
- [Execution Environments](https://docs.ansible.com/automation-controller/latest/html/userguide/execution_environments.html) - not specifically covered in this workshop because the built-in Ansible Execution Environments already included all the Red Hat supported collections which includes all the network collections we use for this workshop.  Execution Environments are container images that can be utilized as Ansible execution.

This exercise will set up your environment for the remaining exercise in the workshop, and includes steps to set up a version control system, which is used to track and provide control over changes made to the automation code. Version control (sometimes called source control) plays an important role in any development project, including automation development. This repository will be used with both the command line utilities and when we use the Ansible Automation Platform.


## Diagram

![Red Hat Ansible Automation](https://github.com/ansible/workshops/raw/devel/images/ansible_network_diagram.png)



## Guide

### Step 1 - Connecting via VS Code

<table>
<thead>
  <tr>
    <th>It is highly encouraged to use Visual Studio Code to complete the workshop exercises. Visual Studio Code provides:
    <ul>
    <li>A file browser</li>
    <li>A text editor with syntax highlighting</li>
    <li>A in-browser terminal</li>
    </ul>
    Direct SSH access is available as a backup, or if Visual Studio Code is not sufficient to the student.  There is a short YouTube video provided if you need additional clarity: <a href="https://youtu.be/Y_Gx4ZBfcuk">Ansible Workshops - Accessing your workbench environment</a>.
</th>
</tr>
</thead>
</table>

- Connect to Visual Studio Code from the Workshop launch page (provided by your instructor).  The password is provided below the WebUI link.

  ![launch page](images/launch_page.png)

- Type in the provided password to connect.

  ![login vs code](images/vscode_login.png)


### Step 2 - Using the Terminal

- Open a terminal in Visual Studio Code:

  ![picture of new terminal](images/vscode-new-terminal.png)

### Step 3 - Create a Git repository

**Everything in this workshop will be done out of your own repo, so you will have it to refer back to after the workshop is complete.**

Open a new browser window (or tab), and login to Gitlab (or Github, but the example here will use GitLab) as yourself.  If you don't have a GitLab account, create one.

Go to https://gitlab.com/redhatautomation/ansible-network-automation-workshop-201/


In the upper right corner, click the forks icon, and select "New project/repository".  


Fill out the form for a forked project / repo, and use the following information:
- Project Name: <span style="color:red">**student-repo**</span> 
- Group or Namespace: {your user id}  
- Project slug: **student-repo** (auto-populated from Project Name)  
- Visibility Level: Public  

**All follow-on exercises expect you to be using the student-repo project.**

Click "Fork project" button at the bottom of the form.


### Step 4 - Create a Token
On the left hand side of your forked project select:  **Settings -> Access Tokens**  

Select **Add a new token** on the right side of the screen, and create a token with the following information:

- Token Name: **Network Automation Workshop**  
- Expiration date: **{today +2 days}**
- Role: **Maintainer**
- Select Scopes:
  - api
  - read_api
  - read_repository
  - write_repository

Click **Create project access token**

You will get an update on your screen in green with "Your new project access token".  Create a file in the student home directory called "gitlab_token.txt", and copy the token into the file (**remember to save the file**), or use the below snippet in the terminal window.  

```bash
[student@ansible-1 ~]$ echo "paste token here" > ~/gitlab_token.txt
```
> **IMPORTANT** - you will need this token in the following steps, and on day 2.


### Step 5 - Clone the Repository:

Go back to your Visual Studio window (or tab) and clone the **student-repo** repository you just created (your fork).

```bash
[student@ansible-1 ~]$ git clone https://gitlab.com/{ your login ID }/student-repo.git
Cloning into 'student-repo'...
remote: Enumerating objects: 1425, done.
remote: Counting objects: 100% (478/478), done.
remote: Compressing objects: 100% (463/463), done.
remote: Total 1425 (delta 170), reused 0 (delta 0), pack-reused 947
Receiving objects: 100% (1425/1425), 9.61 MiB | 20.59 MiB/s, done.
Resolving deltas: 100% (558/558), done.

[student@ansible-1 ~]$ cd student-repo

[student@ansible-1 student-repo]$ pwd
/home/student/student-repo
```

You can also set up Git now:
```bash

[student@ansible-1 ~]$ git config --global user.name "Your Name"
[student@ansible-1 ~]$ git config --global user.email "Your email address"
```

If your Gitlab token is still in your clipboard cache, great.  Otherwise copy it from the file you created earlier.
```bash
[student@ansible-1 ~]$ git remote set-url origin \
https://{gitlab userid}:{git token}@gitlab.com/{gitlab userid}/student-repo.git
```

Fix a problem with the workshop crypto policy

```bash
[student@ansible-1 ~]$ sudo -i
[root@ansible-1 ~]$ update-crypto-policies --set LEGACY
[root@ansible-1 ~]$ exit
[student@ansible-1 ~]$
```
While it will tell you to reboot, you don't need to for this workshop.

### Step 6 - Open the project directory in Visual Studio Code

Click on the files icon in the upper right corner of your Visual Studio Code window, and click `Open Folder`.

![picture of open folder](images/open_folder.png)

In the pop-up window, select `OK` to open the `/home/student` folder.

![picture of open folder 2](images/open_folder_window.png)

In the follow-on window about trusting the folder, click the checkbox stating you trust the authors of these files and then click the `Yes I trust the authors` button.

![picture of trust](images/open_folder_trust.png)

After you have set your remote, you will be able to easily commit code in the Visual Studio Code development environment.  To validate this, create a new file in the student-repo directory. 

You can right-click on the student-repo directory in your visual studio environment, and name the file `test_file.txt`

![create a new file](images/create_new_file.png)

![write in the file](images/testfile.png)

Save the file by going to `File > Save`, or just using the `ctrl-s` key combination.  After you have done this, you can click on the Git button on the left side of the window (third down - see picture).  You will see in the Git window that there is a change to be committed.

![git](images/file_git.png)

Add in a message such as "this is just a test" and click `Commit`.  The button will change to `Sync Changes`.  Click this, and if you have set up your remote settings correctly, it will sync your change to your remote (Gitlab).  If this didn't work, review the earlier steps, or ask your instructor for help.


## Complete

You have completed lab exercise 1 - Getting Started!  

You now understand:

* How to create a git repo in GitLab
* How to create a token
* How to connect to the lab environment with Visual Studio Code
* How to use the `git` command to clone a repository
* How to make changes and then sync them to your remote (Gitlab)
  
  

---
[Next Exercise](../2-RolesAndCollections/README.md)

[Click Here to return to the Ansible Network Automation Workshop](../README.md)