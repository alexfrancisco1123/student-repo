# Ansible Network Automation Workshop

**Read this in other languages**: ![uk](https://github.com/ansible/workshops/raw/devel/images/uk.png) [English](README.md),  ![japan](https://github.com/ansible/workshops/raw/devel/images/japan.png) [日本語](README.ja.md), ![Español](../../images/es.png) [Español](README.es.md).

**This is documentation for Ansible Automation Platform 2**

The Ansible Network Automation workshop is a comprehensive beginners guide to automating popular network data center devices from Arista, Cisco and Juniper via Ansible playbooks. You’ll learn how to pull facts from devices, build templated network configurations, and apply these concepts at scale with Ansible automation controller. You’ll put it all together by exploring the controller’s job templates, surveys, access controls and more.

## Presentation

Want the Presentation Deck?  Its right here:
- [Ansible Network Automation Workshop Deck](https://ansible.github.io/workshops/decks/ansible_network.pdf) PDF
- [Google Source](https://docs.google.com/presentation/d/1PIT-kGAGMVEEK8PsuZCoyzFC5CIzLBwdnftnUsdUNWQ/edit?usp=sharing) for Red Hat employees

## Ansible Network Automation Exercises (Day 1)

* [Exercise 1 - Getting Started](./1-GettingStarted/README.md)
* [Exercise 2 - Roles and Collections](./2-RolesAndCollections/README.md)
* [Exercise 3 - Inventories](./3-Inventories/README.md)
* [Exercise 4 - Playbooks](./4-Playbooks/README.md)
* [Exercise 5 - Variables and Facts](./5-VariablesAndFacts/README.md)
* [Exercise 6 - Logic](./6-Logic/README.md)
* [Exercise 7 - Resource Modules](./7-ResourceModules/README.md)



## Network Diagram

![Red Hat Ansible Automation](https://github.com/ansible/workshops/blob/devel/images/ansible_network_diagram.png?raw=true)

---
![Red Hat Ansible Automation](https://github.com/ansible/workshops/blob/devel/images/rh-ansible-automation-platform.png?raw=true)
